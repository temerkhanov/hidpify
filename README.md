# HIDPIFY

A script to automate (my) HiDPI settings. 

Currently only for KDE Plasma.

Automates KDE font DPI, SDDM DPI, QT scaling, GTK scaling, Firefox and Thunderbird 'pixels per inch'.

### Download:

```shell
curl -O https://gitlab.com/cscs/hidpify/raw/master/hidpify.sh
```

### Mark Executable:

```shell
chmod +x hidpify.sh
```

### Run:

```shell
./hidpify.sh -i
```

### Remove (Undo)

```shell
./hidpify.sh -r
```

> Note: If you wish to try a different DPI setting after first run, it may be advisable to execute 'remove' _then_ 'install' the new DPI.

### Donate  

Everything is free, but you can donate using these:  

<a href='https://ko-fi.com/X8X0VXZU' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://az743702.vo.msecnd.net/cdn/kofi4.png?v=2' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a> &nbsp; <a href='https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=M2AWM9FUFTD52'><img height='36' style='border:0px;height:36px;' src='https://gitlab.com/cscs/resources/raw/master/paypalkofi.png' border='0' alt='Donate with Paypal' />  
